NodeJS - Web Scrapper
-----------------------------------------------------------------------------------------------------------------
This is a web scrapper written in NodeJS and it scrapes the title, release year, rank and categories of all the top 100 TV Series/Shows on IMDB. Puppeteer was used to scrape the data in this program and the result is then written to json file. In order to run this program, have vs code and node js installed on your computer. after unzipping the file, open the console inside the project and type the following:

### Project Setup
```
npm install 
```

### Run Program
```
node index.js
```


look at the output.json for the result after scrapped the web page. 