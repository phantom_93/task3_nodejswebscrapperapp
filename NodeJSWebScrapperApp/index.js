const puppeteer = require('puppeteer');
const fs = require('fs');

// function to write the scrapped result as json file 
function writetojson(content){
    fs.writeFile("output.json", JSON.stringify(content,null,1), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("JSON file was created!");
    }); 
}

function webscrapper(){
(async () => {
// web page to scrape 
  let movieUrl = "https://www.imdb.com/list/ls057886464/";
  // launch the browser 
  let browser = await puppeteer.launch();
  // start a new page 
  let page = await browser.newPage();
  // tell the browser when the navigation is finished when there are no more than two network connections
  // for at least half of a second 
  await page.goto(movieUrl, {waitUntil: 'networkidle2'});
  let data = await page.evaluate(()=>{
      // create queries for titles, ranks and categories for top 100 tv series at IMDB 
      let seriesTitles = document.querySelectorAll('.lister-item-header');
      let seriesRanks = document.querySelectorAll('.ipl-rating-star.small');
      let seriesCategories = document.querySelectorAll('.genre');
      let seriesObjects = [];
      // loop through the array of the seriesObjects in order to get the title, rank and categories of all top 100 tv series
      for (let index = 0; index < seriesTitles.length; index++) {
          seriesObjects[index] = {
                title: seriesTitles[index].innerText,
                rank: seriesRanks[index].innerText,
                categorie: seriesCategories[index].innerText
              
          }
          
      }
      return seriesObjects;
  });
  
  console.log(data);
  // add data to write to json function to write the res
  writetojson(data);
  await browser.close(); // close the browser
  
})();

}


webscrapper(); 
